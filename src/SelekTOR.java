/*
 * Copyright (C) 2014 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Explanation of why this is in the default package
 *
 * @author Alistair Neil <info@dazzleships.net>
 *
 * The app launcher is here in the default package which should not normally be
 * used. The method java uses to launche its apps is not fully compatible with
 * various Linux launchers panels such as Dockbarx or Unity launchers and thus
 * confuses those apps. By putting this simple launch handle here in the default
 * package the Linux dockpanel launchers should function correctly when it
 * commes to application pinning.
 *
 */
public class SelekTOR {

    /**
     * @param args the command line arguments
     */
    public static void main(final String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new client.SelekTOR(args);
            }
        });
    }

}
