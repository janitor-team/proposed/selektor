/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lib;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;

/**
 *
 * @author Alistair Neil <info@dazzleships.net>
 */
public class TrayPopupMenu extends JPopupMenu {

    private Color bg;

    public TrayPopupMenu() {
        super();
        bg = super.getBackground();
    }

    @Override
    public JMenuItem add(JMenuItem mi) {
        mi.setForeground(new Color(UIManager.getColor("MenuItem.foreground").getRGB()));
        super.add(mi);
        return mi;
    }

    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        this.bg = bg;
    }

    @Override
    public void paintComponent(final Graphics g) {
        g.setColor(new Color(bg.getRGB()));
        g.fillRect(0, 0, getWidth(), getHeight());
    }

}
