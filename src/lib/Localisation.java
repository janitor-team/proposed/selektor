/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package lib;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Alistair Neil, <info@dazzleships.net>
 */
public final class Localisation {

    // Private Stuff
    private final ResourceBundle resourceBundle;
    private String encoding = null;

    public Localisation(String resource) {
        String strSupportedLangs = "fr,en,pt";
        encoding = this.getString("encoding");
        if (!strSupportedLangs.contains(getLocale().getLanguage())) {
            Locale.setDefault(new Locale("en", "GB"));
            resourceBundle = ResourceBundle.getBundle(resource, Locale.getDefault());
        } else {
            resourceBundle = ResourceBundle.getBundle(resource);
        }
    }

    /**
     * Get the locale
     *
     * @return The locale
     */
    public Locale getLocale() {
        return Locale.getDefault();
    }

    public String toWebLanguageTag() {
        return getLocale().toLanguageTag().replace('-', '_');
    }

    /**
     * Get the country name for supplied iso2 code
     *
     * @param iso2
     * @return Localised country name
     */
    public String getDisplayCountry(String iso2) {
        switch (iso2) {
            case "A1":
            case "A2":
            case "O1":
            case "U1":
                return getString("iso" + iso2);
            default:
                Locale obj = new Locale("", iso2);
                return obj.getDisplayCountry();
        }
    }

    /**
     * Get localised string using given key
     *
     * @param key
     * @return localised string
     */
    public String getString(String key) {
        try {
            if (!encoding.contentEquals("encoding")) {
                byte[] text = resourceBundle.getString(key).getBytes("ISO8859-1");
                return new String(text, encoding);
            } else {
                return resourceBundle.getString(key);
            }
        } catch (Exception ex) {
        }
        return key;
    }

    /**
     * Get an array of localised strings using a key array or multiple keys
     *
     * @param keys
     * @return localised string array
     */
    public String[] getStrings(String... keys) {

        String[] result = new String[keys.length];
        int i = 0;
        for (String key : keys) {
            result[i++] = getString(key);
        }
        return result;
    }
}
