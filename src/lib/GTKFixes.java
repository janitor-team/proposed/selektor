/*
 * Copyright (C) 2009-2017 Alistair Neil <info@dazzleships.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lib;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.event.MenuEvent;

/**
 *
 * @author Alistair Neil <info@dazzleships.net>
 */
public class GTKFixes {

    public static void fixMenubarHeight(JMenuBar jmb, JMenuItem jmi) {
        jmb.setPreferredSize(new Dimension(jmb.getPreferredSize().width, jmi.getPreferredSize().height));
    }

    /**
     * Fixes incorrect JMenu selectionForeground and selectionBackground
     * highlighting
     *
     * @param jms Array of JMenus or single JMenu
     */
    public static void fixMenuSelectionColor(JMenu... jms) {
        // If gtk laf not installed then do nothing
        if (!UIManager.getLookAndFeel().getID().contentEquals("GTK")) {
            return;
        }

        for (final JMenu jm : jms) {
            final Color bgfixed = new Color(((Color) UIManager.get("Menu.selectionBackground")).getRGB());
            final Color fgfixed = new Color(((Color) UIManager.get("Menu.selectionForeground")).getRGB());
            final Color fgnormal = jm.getForeground();
            final Color bgnormal = new Color(((Color) UIManager.get("Menu.background")).getRGB());
            jm.setText(" " + jm.getText() + " ");
            jm.addMenuListener(new javax.swing.event.MenuListener() {

                @Override
                public void menuSelected(MenuEvent e) {
                    jm.setOpaque(true);
                    jm.setForeground(fgfixed);
                    jm.setBackground(bgfixed);
                }

                @Override
                public void menuDeselected(MenuEvent e) {
                    jm.setOpaque(false);
                    jm.setForeground(fgnormal);
                    jm.setBackground(bgnormal);
                }

                @Override
                public void menuCanceled(MenuEvent e) {
                    jm.setOpaque(false);
                    jm.setForeground(fgnormal);
                    jm.setBackground(bgnormal);
                }
            });
        }
    }

    /**
     * Fixes incorrect JMenu border outline
     *
     * @param jms Array of JMenus or single JMenu
     */
    public static void fixJMenuPopupBorder(JMenu... jms) {
        // If gtk laf not installed then do nothing
        if (!UIManager.getLookAndFeel().getID().contentEquals("GTK")) {
            return;
        }
        for (final JMenu jm : jms) {
            jm.getPopupMenu().setBorder(BorderFactory.createLineBorder(
                    new Color(UIManager.getColor("PopupMenu.background").getRGB()).darker(), 1));
        }
    }

    public static void fixTrayMenuPopupBorder(TrayPopupMenu tpm) {
        tpm.setBorder(BorderFactory.createLineBorder(
                new Color(UIManager.getColor("PopupMenu.background").getRGB()).darker(), 1));
    }

    /**
     * Fixes incorrect JMenu selectionForeground highlighting
     *
     * @param jmi Array of JMenuItems or single JMenuItem
     */
    public static void fixMenuItemFgColor(JMenuItem... jmi) {
        // If gtk laf not installed then do nothing
        if (!UIManager.getLookAndFeel().getID().contentEquals("GTK")) {
            return;
        }
        for (final JMenuItem jm : jmi) {
            final Color fgfixed = new Color(UIManager.getColor("MenuItem.foreground").getRGB());
            jm.setForeground(fgfixed);
        }
    }

    public static void fixTextAreaColor(JTextArea jta) {
        if (OSFunction.isLinux()) {
            jta.setForeground(UIManager.getColor("JLabel.foreground"));
            jta.setBackground(UIManager.getColor("JLabel.background"));
        }
    }

}
